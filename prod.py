#!/usr/bin/env python3
import os
from pathlib import Path

import torch
from bottle import post, request, run, BaseRequest
import random

from helper.ActionHandler import ActionHandler
from helper.dataprep import DataPreparer



@post("/")
def index():
    game = request.json


    if isGameFinished(game):
        print(f'round: {game["round"]}, outcome: {game["outcome"]}, Points: {game["points"]}')
        return

    gameStateTensor = dataPreparer.convertGameDataToTensor(game, [])

    if model is not None:
        actions = model(gameStateTensor)

        # best_choice = chooseBestPossibleAction(actions, game)
        global lastActionIndex, lastGame

        # lastActionIndex = best_choice
        # lastGame = game

        possActionArray = actionHandler.generatePossibleActionsArray(game)
        #print("Net Prediction: ", actions.max(0)[1])
        best_choice = chooseBestPossibleAction(actions, possActionArray, game)
        print("Choosen Action: ", best_choice)

        action = convertActionIndexToAction(game, best_choice)

        print(f'round: {game["round"]}, outcome: {game["outcome"]}, Points: {game["points"]}, Action: {best_choice}')

        return action
    else:
        print("Model not found")
        return

def chooseBestPossibleAction(actions, possActionArray, gameState):
    evaluation = actions.detach().numpy()
    evalWithIndex = []
    for index, action in enumerate(evaluation):
        evalWithIndex.append((index, action))
    evalWithIndex = sorted(evalWithIndex, key=lambda x: x[1], reverse=True)
    for actionTuple in evalWithIndex:
        actionIndex = actionTuple[0]
        if possActionArray[actionIndex] == 1:
            if actionHandler.getActionCosts([])[actionIndex] <= gameState.get("points"):
                return actionIndex




def isGameFinished(game):
    return game.get("outcome") in ["win", "loss"]


def load_model(path):
    my_file = Path(path)
    if my_file.is_file():
        print("Load model")
        return torch.load(path, map_location=lambda storage, loc: storage)
    else:
        print("File not found")
        return None

def getRandomCity(game):
    cities = game["cities"].values()
    cities = list(cities)
    city = random.choice(cities)
    return city

def getRandomInfectedCity(game):
    cities = game["cities"].values()
    cities = list(filter(lambda city: len(city.get("events", [])) > 0, cities))
    cities = list(filter(lambda city: len(list(filter(lambda event: event.get("type") == "outbreak", city.get("events")))) != 0, cities))
    city = random.choice(cities)
    return city

def convertActionIndexToAction(game, actionIndex):
    city1 = getRandomCity(game).get("name")
    city2 = getRandomCity(game).get("name")
    while not city1 != city2:
        city2 = getRandomCity(game).get("name")
    infectedCity = getRandomInfectedCity(game)

    return actionHandler.getActionString(actionIndex, city1, city2, infectedCity)



dataPreparer = DataPreparer()
actionHandler = ActionHandler()

#name = "model"
name="model-1zu3"
#name="model-loose"

path = os.path.join(os.getcwd(), "trained_models", name)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = load_model(path)

BaseRequest.MEMFILE_MAX = 10 * 1024 * 1024

run(host="localhost", port=50123, quiet=True)