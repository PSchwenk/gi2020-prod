import torch.nn as nn
import torch.nn.functional as F


class Network(nn.Module):
    def __init__(self):
        super(Network, self).__init__()

        self.fc1 = nn.Linear(in_features=2885, out_features=5000)
        #self.fc1 = nn.Linear(in_features=2873, out_features=5000)
        #self.fc2 = nn.Linear(in_features=5000, out_features=2500)
        #self.fc3 = nn.Linear(in_features=2500, out_features=1000)
        #self.fc4=nn.Linear(in_features=1000,out_features=260)

        self.fc2_2 = nn.Linear(in_features=5000, out_features=1000)
        self.fc3_2 = nn.Linear(in_features=1000, out_features=100)
        self.dropout1 = nn.Dropout(p=0.30)
        self.fc4_2 = nn.Linear(in_features=100, out_features=200)
        self.fc5_2 = nn.Linear(in_features=200, out_features=24)
        self.fc6_2=nn.Linear(in_features=24,out_features=12)

    # def _createweights(self):
    #     for m in self.modules():
    #         if isinstance(m, nn.Linear):
    #             nn.init.uniform(m.weight, -0.01, 0.01)
    #             nn.init.constant(m.bias, 0)

    def forward(self, input):
        output  = F.relu(self.fc1(input))
        output = F.relu(self.fc2_2(output))
        output = F.relu(self.fc3_2(output))
        output = self.dropout1(output)
        output = F.relu(self.fc4_2(output))
        output = F.relu(self.fc5_2(output))
        action = F.relu(self.fc6_2(output))

        #city    = self.fc4_2(output)

        return action