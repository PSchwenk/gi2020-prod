## Informaticup 2020 (Pandemie)
---

### Installation Instructions 



1. Clone Repository
1. Run `pip install -r requirements.txt` in directory
1. (Maybe use conda to install pytorch with cuda support in your python enviroment if you are using windows)
4. Run file `./prod.py` with python
5. Start your Comandline tool available at `https://github.com/informatiCup/informatiCup2020/releases/tag/2.1.0`

---
### Information


- Server IP Adress: `localhost`
- Server port: `50123`

---

Authors: Robert Manschke (Nordakademie) & Patrick Schwenk (Nordakademie)

