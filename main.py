import sys
from threading import Thread

from rx.subject import Subject

from helper.Logger import Logger
from helper.server import Server
from train import Trainer




def train():
    logger = Logger(visible=True)

    gameStateSubject = Subject()
    actionSubject = Subject()

    Trainer(gameStateSubject, actionSubject, logger)
    Thread(target=lambda: Server(gameStateSubject, actionSubject)).start()



if __name__ == "__main__":
    train()