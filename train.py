import datetime
import os
import random
from collections import namedtuple
from pathlib import Path
import math
import torch
from torch import optim

from game import Game
from helper import Logger
from helper.ActionHandler import ActionHandler
from helper.dataprep import DataPreparer
from helper.ReplayMemory import ReplayMemory
from network import Network
from rx.subject.subject import Subject
import torch.nn.functional as F
import matplotlib.pyplot as plt


class Trainer():
    def __init__(self, gameStateSubject: Subject, actionSubject: Subject, logger: Logger):
        self.actionHandler: ActionHandler = ActionHandler()
        self.STATS_FILE_PATH = os.path.join(os.getcwd(), "src", "stats", 'log.txt')
        self.TARGET_MODEL_SAVE_PATH = "./{}/model".format("trained_models")

        self.gameStateSubject: Subject = gameStateSubject
        self.actionSubject: Subject = actionSubject
        self.logger: Logger = logger
        self.lastGame: Game = None
        self.currentGame: Game = None
        self.trainPrepared: bool = False
        self.newRound: bool = False

        self.gameStateSubject.subscribe(lambda gameObj: self.trainLoop(gameObj))
        self.dataPreparer: DataPreparer = DataPreparer()

        self.batch_size = 0

        self.t = 0
        self.episodesArray = []
        self.lossArray = []

        self.train_data_list = []
        self.train_data = []
        self.reward_list = []
        self.impossibleActionsThisRound = []

        self.roundMaxPopulation = 700000

    def prepareTrainer(self):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.GAMMA = 0.999
        self.EPS_START = 0.85
        self.EPS_END = 1e-6
        self.EPS_DECAY = 2000
        self.TARGET_UPDATE = 10

        self.num_episodes = 50

        # Get number of actions
        self.n_actions = 12

        self.winsCount = 0
        self.lossesCount = 0

        self.cityEvents = {}
        self.globalEvents = {}

        self.usePrelearned = True

        self.policy_net = self.load_model("./{}/model{}".format("trained_models", ""))
        if self.policy_net is None or self.usePrelearned == False:
            self.policy_net = Network().to(self.device)
            self.logger.print("New Model")
        else:
            self.logger.print("Model loaded")

        self.target_net = Network().to(self.device)
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()

        self.optimizer = optim.RMSprop(self.policy_net.parameters())

        self.Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward'))
        self.memory = ReplayMemory(self.Transition)

        self.steps_done = 0
        self.episode_durations = []

        self.trainPrepared = True

    def plot_stats(self):
        plt.clf()
        plt.ioff()

        fig = plt.figure()
        fig.add_subplot(211)

        plt.title('Training...')
        plt.xlabel('Episode')
        plt.ylabel('Loss')
        if len(self.episodesArray) > 200 and len(self.lossArray) > 200:
            plt.plot(self.episodesArray[-200:], self.lossArray[-200:])
        else:
            plt.plot(self.episodesArray, self.lossArray)

        fig.add_subplot(212)

        width = 0.35
        plt.bar(0, self.winsCount, width, label='Wins')
        plt.bar(1, self.lossesCount, width, label='Losses')

        dir_path = os.getcwd()

        file = os.path.join(str(dir_path), "src", "stats",
                            "stats" + str(datetime.datetime.now().timestamp()) + '-' + str(
                                self.currentGame.getGameState().get("round")) + '.png')
        if not os.path.exists(os.path.dirname(file)):
            os.makedirs(os.path.dirname(file))
        plt.savefig(file)
        plt.close()

    def select_action(self, gameObj: Game, chosenActions):
        sample = random.random()
        if not self.isFirstGame():
            lastRoundError = self.lastGame.getGameState().get("error") != None
        else:
            lastRoundError = False


        eps_threshold = self.EPS_END + (self.EPS_START - self.EPS_END) * math.exp(
            -1. * self.steps_done / self.EPS_DECAY)
        self.logger.print("EPS:", eps_threshold)

        gameStateTensor = gameObj.getGameTensor().to(self.device)

        if sample > eps_threshold:
            self.logger.print("Prediction")
            with torch.no_grad():
                prediction = self.policy_net(gameStateTensor)
                #best_choice = self.chooseBestPossibleAction(prediction, gameObj.getGameState())
                best_choice = prediction.max(0)[1]
                if lastRoundError and self.lastActionIndex == best_choice:
                    # Random
                    return torch.tensor([[random.randrange(self.n_actions)]], device=self.device, dtype=torch.long)
                else:
                    return torch.tensor([[best_choice]], device=self.device, dtype=torch.long)

        else:
            self.logger.print("Random")
            return torch.tensor([[random.randrange(self.n_actions)]], device=self.device, dtype=torch.long)

    def chooseBestPossibleAction(self, actions, gameState):
        evaluation = actions.cpu().numpy()
        evalWithIndex = []
        for index, action in enumerate(evaluation):
            evalWithIndex.append((index, action))
        evalWithIndex = sorted(evalWithIndex, key=lambda x: x[1], reverse=True)
        for actionTuple in evalWithIndex:
            if self.enoughPointsForAction(actionTuple[0], gameState):
                if (actionTuple[0] == 0) or \
                        not ((actionTuple[0] == self.lastActionIndex) \
                             and (self.lastGame.getGameState().get("points") == self.currentGame.getGameState().get(
                                    "points")) \
                             and (not self.newRound)):
                    return actionTuple[0]
                else:
                    self.impossibleActionsThisRound.append(actionTuple[0])
        return 0

    def enoughPointsForAction(self, actionIndex, gameState):
        cost = self.actionHandler.getActionCosts([])[actionIndex]
        availablePoints = gameState.get("points")
        return cost < availablePoints

    def trainLoop(self, gameObj: Game):
        if not self.trainPrepared:
            self.prepareTrainer()

        # Increase Steps
        self.logger.print("Steps:", self.steps_done)
        self.steps_done += 1

        # Reward for last Action

        self.updateGameObjects(gameObj)
        self.updateNewGameVariable()

        self.chosenActions = []

        if not self.isFirstGame():
            if self.lastGame.getGameState().get("round") != self.currentGame.getGameState().get("round"):
                self.newRound = True
                self.impossibleActionsThisRound = []
            else:
                self.newRound = False

            reward = self.generateReward(self.lastGame, self.currentGame)
            print("Reward for Round ", self.currentGame.getGameState().get("round"), ":", reward)
            reward = torch.tensor([reward], device=self.device)

            self.printEvents()

            self.memory.push(
                self.lastGame.getGameTensor().to(self.device),
                self.lastActionIndex,
                self.currentGame.getGameTensor().to(self.device), reward)
            self.batch_size += 1

        if self.newRound and not self.newGame and self.isFirstGame():
            self.optimize_model()

        self.logger.print()
        # END FOR last game
        ############################################################################################
        # START FOR new Game

        if not self.isGameFinished(self.currentGame):
            # Select/Predict Action
            self.lastActionIndex = self.select_action(self.currentGame, self.chosenActions)
            runnableAction = self.convertActionIndexToAction(self.lastActionIndex)

            # Return Action
            self.actionSubject.on_next(runnableAction)

        else:  # GameFinished
            # Optimize, plot & Save
            self.saveStatsFile(gameObj.getGameState(), self.STATS_FILE_PATH)
            self.optimize_model()
            self.resetMemoryAndBatchSize()
            self.target_net.load_state_dict(self.policy_net.state_dict())

            self.updateWinsAndLossesCount(self.currentGame.getGameState())
            self.plot_stats()

            self.save_model(self.target_net, self.TARGET_MODEL_SAVE_PATH)

        self.logger.print("Outcome", gameObj.getGameState().get("outcome"), "Round",
                          gameObj.getGameState().get("round"), "Points", gameObj.getGameState().get("points"))

    def isFirstGame(self):
        return self.lastGame is None

    def isGameWon(self, gameState):
        return gameState.get("outcome") == 'win'

    def updateWinsAndLossesCount(self, gameState):
        if self.isGameWon(gameState):
            self.winsCount += 1
        else:
            self.lossesCount += 1

    def resetMemoryAndBatchSize(self):
        self.batch_size = 0
        self.memory.clear()

    def saveStatsFile(self, gameState, txt_file_path):
        if not os.path.exists(os.path.dirname(txt_file_path)):
            os.makedirs(os.path.dirname(txt_file_path))
        txt_file = open(txt_file_path, "a+")
        txt_file.write("Round: " + str(gameState.get("round")))
        txt_file.write(" Outcome: " + str(gameState.get("outcome")))
        txt_file.write(" Points: " + str(gameState.get("points")))
        txt_file.write("\n")
        txt_file.close()

    def updateNewGameVariable(self):
        if self.lastGame is None:
            self.newGame = True
        else:
            if self.lastGame.getGameState().get("round") > self.currentGame.getGameState().get("round"):
                self.newGame = True
                self.lastGame = None
            else:
                self.newGame = False

    def updateGameObjects(self, gameObj: Game):
        self.lastGame = self.currentGame
        self.currentGame = gameObj

    def printEvents(self):
        cities = self.currentGame.getGameState()["cities"].values()
        for city in cities:
            for event in city.get("events", []):
                type = event.get("type")
                if type in self.cityEvents:
                    self.cityEvents[type] + 1
                else:
                    self.cityEvents[type] = 1
        for event in self.currentGame.getGameState().get("events", []):
            type = event.get("type")
            if type in self.globalEvents:
                self.globalEvents[type] + 1
            else:
                self.globalEvents[type] = 1
        self.logger.print("City events", self.cityEvents)
        self.logger.print("Global events", self.globalEvents)

    def optimize_model(self):
        self.logger.print("Optimize Model")
        transitions = self.memory.sample(self.batch_size)
        batch = self.Transition(*zip(*transitions))

        non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                                batch.next_state)), device=self.device, dtype=torch.bool)

        non_final_next_states = torch.stack([s for s in batch.next_state if s is not None])
        state_batch = torch.stack(batch.state)
        action_batch = torch.cat(batch.action)
        reward_batch = torch.cat(batch.reward)

        state_action_values = self.policy_net(state_batch).gather(1, action_batch)

        next_state_values = torch.zeros(self.batch_size, device=self.device)
        next_state_values[non_final_mask] = self.target_net(non_final_next_states).max(1)[0].detach()

        # Compute the expected Q values
        expected_state_action_values = (next_state_values * self.GAMMA) + reward_batch

        # Compute Huber loss
        loss = F.smooth_l1_loss(state_action_values, expected_state_action_values.unsqueeze(1))

        # Optimize the model
        self.optimizer.zero_grad()
        loss.backward()
        for param in self.policy_net.parameters():
            if param.grad is not None:
                param.grad.data.clamp_(-1, 1)
        self.optimizer.step()

        self.t += 1
        self.episodesArray.append(self.t)
        self.lossArray.append(loss)

    def save_model(self, model, path):
        my_file = Path(path)
        my_file.parent.mkdir(parents=True, exist_ok=True)
        torch.save(model, path)

    def load_model(self, path):
        my_file = Path(path)
        if my_file.is_file():
            return torch.load(path)
        else:
            return None

    def isGameFinished(self, gameObj: Game):
        return gameObj.getGameState().get("outcome") in ["win", "loss"]

    def generateReward(self, lastGameObj: Game, currentGameObj: Game):
        reward = 2.

        self.currentGame.getGameState().get("round")

        newCities = currentGameObj.getGameState()["cities"].values()
        oldCities = lastGameObj.getGameState()["cities"].values()
        popNew = self.sumPopulation(newCities)
        popOld = self.sumPopulation(oldCities)

        infNew = self.sumPopulationInfected(newCities)
        infOld = self.sumPopulationInfected(oldCities)

        populationChangeReward = (popNew - popOld) / max(popNew, popOld, 1) * 100.
        if populationChangeReward != 0:
            reward = (reward + populationChangeReward) / 2.

        infectedChangeReward = (infOld - infNew) / max(infNew, infOld, 1) * 100.
        if infectedChangeReward != 0:
            reward = (reward + infectedChangeReward) / 2.

        # Aktion fehlgeschlagen
        if lastGameObj.getGameState().get("error") != None:
            reward = -2.

        #Verloren
        if self.isGameFinished(self.currentGame):
            rounds = self.currentGame.getGameState().get("round")
            points = self.currentGame.getGameState().get("points")
            if self.currentGame.getGameState().get("outcome") == "loss":
                reward = (-200. * (1 / math.pow(rounds*10, 0.1))) - (-200. * (1 / math.pow(points, 0.1)))
                # reward = -20. * 50. / self.currentGame.getGameState().get( "round") - self.currentGame.getGameState().get("points")

        #Gewonnen
            else:
                # reward = 20. * 100. / self.currentGame.getGameState().get("round")
                reward = 200. * math.pow(0.95,  rounds)
        return reward

    def isNewGame(self, lastGameState, newGameState):
        if (lastGameState == None):
            return True
        else:
            return (lastGameState["round"] == newGameState["round"])

    def sumPopulation(self, cities):
        population = 0
        for city in cities:
            population += city.get("population")
        return population

    def sumPopulationInfected(self, InfectedCities):
        population = 0
        for city in InfectedCities:
            for event in city.get("events", []):
                if event.get("type") == "outbreak":
                    population += event.get("prevalence") * city.get("population")
        return population

    def getRandomCity(self):
        cities = self.currentGame.getGameState()["cities"].values()
        cities = list(cities)
        city = random.choice(cities)
        return city

    def getInfectedCities(self, cities):
        cities = list(filter(lambda city: "outbreak" in list(map(lambda event: event.get("type"), city.get("events", []))) , cities))
        return cities

    def getNotInfectedCities(self, cities):
        cities = list(filter(lambda city: "outbreak" not in list(map(lambda event: event.get("type"), city.get("events", []))) , cities))
        return cities



    def getMostDangerousCities(self, cities, filterArray):
        # infected
        # pop max
        # conn max

        infectedCities = self.getInfectedCities(cities)
        for filter in filterArray:
            notInfectedCities = self.filterCities(notInfectedCities, filter)
        #infectedCities = list(filter(lambda city: "medicationDeployed" not in list(map(lambda event: event.get("type"), city.get("events", []))), infectedCities))
        infectedCities = sorted(infectedCities, key=lambda city: (city['population'], len(city['connections'])), reverse=True)
        return infectedCities




    def getMostEndangeredCities(self, cities, filterArray):
        # not infected
        # pop max
        # conn max
        # medDeployed 0
        # antiVaccina

        notInfectedCities = self.getNotInfectedCities(cities)
        for filter in filterArray:
            notInfectedCities = self.filterCities(notInfectedCities, filter)
        #"antiVaccinationism"
        #notInfectedCities = self.filterCities(notInfectedCities, "medicationDeployed")
        notInfectedCities = sorted(notInfectedCities, key=lambda city: (city['population'], len(city['connections'])), reverse=True)
        return notInfectedCities


    def filterCities(self, cities, eventName):
        cities = list(filter(lambda city: eventName not in list(map(lambda event: event.get("type"), city.get("events", []))), cities))
        return cities

    def chooseCityWithPathogen(self, cities, eventName, pathogenName):
        cities = list(
            filter(lambda city: eventName in list(map(lambda event: event.get("type"), city.get("events", []))),
                   cities))
        cities = list(
            filter(lambda city: pathogenName in list(map(lambda event: event.get("pathogen").get("name"), city.get("events", []))),
                   cities))

        cities = sorted(cities, key=lambda city: (city['population'], len(city['connections'])),
                                reverse=True)
        return cities[0]



    def getRandomInfectedCity(self):
        cities = self.currentGame.getGameState()["cities"].values()
        cities = list(filter(lambda city: len(city.get("events", [])) > 0, cities))
        cities = list(filter(
            lambda city: len(list(filter(lambda event: event.get("type") == "outbreak", city.get("events")))) != 0,
            cities))
        city = random.choice(cities)
        return city

    def cityHasPathogen(self, city, pathogenName):
        events = city.get("events",[])
        for event in events:
            if event.get("pathogen") == pathogenName:
                return True
        return False



    def filterCitiesWithPathogenPresent(self, cities, pathogenName):
        cities = list(filter(lambda city: self.cityHasPathogen(city, pathogenName), cities))
        return cities



    def findCityWithLowestAttribute(self, cities, attributeName):
        cities = sorted(cities, key=lambda city: self.attributeConverter.parseNumValue(city.get(attributeName, "")))
        return cities[0]


    def convertActionIndexToAction(self, actionIndex):
        actionIndex = actionIndex.cpu().item()
        print("Chosen Action:", actionIndex)
        city1 = self.getRandomCity().get("name")
        city2 = self.getRandomCity().get("name")
        while not city1 != city2:
            city2 = self.getRandomCity().get("name")
        infectedCity = self.getRandomInfectedCity()

        return self.actionHandler.getActionString(actionIndex, city1, city2, infectedCity)



    # def convertActionIndexToAction(self, actionIndex):
    #     actionIndex = actionIndex.cpu().item()
    #     cities = self.currentGame.getGameState()["cities"].values()
    #
    #     self.logger.print("Chosen Action:", actionIndex)
    #
    #     pathogenName = None
    #     filterArrayCity1 = []
    #     filterArrayCity2 = []
    #     filterArrayCityInfected = []
    #
    #     city2Name = None
    #
    #
    #
    #     if actionIndex == 1:
    #         filterArrayCityInfected.append('quarantine')
    #     elif actionIndex == 2:
    #         filterArrayCityInfected.append('airportClosed')
    #     elif actionIndex == 3:
    #         filterArrayCityInfected.append('connectionClosed')
    #
    #
    #
    #     cities1 = self.getMostEndangeredCities(cities, filterArrayCity1)
    #     infectedCities = self.getMostDangerousCities(cities, filterArrayCityInfected)
    #
    #     if actionIndex == 4:
    #         infectedCity = infectedCities[0]
    #         connectedCities = infectedCity.get("connections")
    #
    #         city2 = self.getMostEndangeredCities(connectedCities, filterArrayCity2)
    #         while not infectedCity != city2:
    #             city2 = self.getMostEndangeredCities(connectedCities, filterArrayCity2)
    #
    #         city2Name = city2.get("name")
    #
    #     city1 = cities1[0]
    #
    #
    #
    #
    #
    #
    #     globalEvents = self.currentGame.getGameState().get("events")
    #
    #
    #     pathogenList = self.actionHandler.getListOfPathogensToEvent(self.currentGame.getGameState(), "outbreak", "CITY")
    #     vacPathogenList = self.actionHandler.getListOfPathogensToEvent(self.currentGame.getGameState(), "vaccineAvailable", "GLOBAL")
    #     medPathogenList = self.actionHandler.getListOfPathogensToEvent(self.currentGame.getGameState(), "medicationAvailable", "GLOBAL")
    #
    #
    #
    #
    #
    #
    #
    #     if actionIndex == 4:
    #
    #         pathogenName = random.choice(list(set(pathogenList) - set(vacPathogenList)))
    #         infectedCity = self.chooseCityWithPathogen(cities, "outbreak", pathogenName)
    #     elif actionIndex == 5:
    #         pathogenName = random.choice(list(set(pathogenList) - set(vacPathogenList)))
    #         city1 = self.filterCitiesWithPathogenPresent(self.getMostEndangeredCities(cities, ["antiVaccinationism"]), pathogenName)[0]
    #     elif actionIndex == 6:
    #         pathogenName = pathogenName = random.choice(list(set(pathogenList) - set(medPathogenList)))
    #         infectedCity = self.chooseCityWithPathogen(cities, "outbreak", pathogenName)
    #     elif actionIndex == 7:
    #         pathogenName = random.choice(list(set(pathogenList) - set(medPathogenList)))
    #         city1 = self.filterCitiesWithPathogenPresent(self.getMostDangerousCities(cities, []),
    #                                                      pathogenName)[0]
    #
    #
    #
    #
    #     elif actionIndex == 8:
    #         city1 = self.findCityWithLowestAttribute(cities, "economy")
    #     elif actionIndex == 9:
    #         city1 = self.findCityWithLowestAttribute(cities, "government")
    #     elif actionIndex == 10:
    #         city1 = self.findCityWithLowestAttribute(cities, "hygiene")
    #     elif actionIndex == 11:
    #         city1 = self.findCityWithLowestAttribute(cities, "awareness")
    #
    #
    #     print(actionIndex, city1.get("name"), city2Name, infectedCity.get("name"), pathogenName)
    #
    #
    #     return self.actionHandler.getActionString(actionIndex, city1.get("name"), city2Name, infectedCity.get("name"), pathogenName)
