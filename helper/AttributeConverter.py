class AttributeConverter():
    def __init__(self):
        pass

    def parseNumValue(self, val):
        switcher = {
            '--': -2,
            '-': -1,
            'o': 0,
            '+': 1,
            '++': 2
        }
        return switcher.get(val)