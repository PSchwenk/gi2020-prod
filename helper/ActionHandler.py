import numpy

from helper.AttributeConverter import AttributeConverter


class ActionHandler():
    def __init__(self):
        self.attributeConverter = AttributeConverter()
        self.ACTION_COUNT = 12
        self.MAX_STATE_CITY_STATS = 2
        self.EVENTCONTEXT = self.enum(GLOBAL="GLOBAL", CITY="CITY")
        self.CITYSTATS = self.enum(ECONOMY="economy", GOVERNMENT="government", HYGIENE="hygiene", AWARENESS="awareness")

    def enum(self, **enums):
        return type('Enum', (), enums)

    def getActionString(self, index, city1, city2, infectedCity):

        pathogenName = list(filter(lambda x: x.get("type") == "outbreak", infectedCity.get("events")))[0].get("pathogen").get("name")

        infectedCity = infectedCity.get("name")

        switcher = {
            0: '{"type": "endRound"}',
            1: '{"type": "putUnderQuarantine", "city": "' + str(infectedCity) + '", "rounds": 1}',
            2: '{"type": "closeAirport", "city": "' + str(infectedCity) + '", "rounds": 1}',
            3: '{"type": "closeConnection", "fromCity": "' + str(infectedCity) + '", "toCity": "' + str(
                city2) + '", "rounds": 1}',
            4: '{"type": "developVaccine", "pathogen":"' + str(pathogenName) + '"}',
            5: '{"type": "deployVaccine", "pathogen": "' + str(pathogenName) + '", "city": "' + str(city1) + '"}',
            6: '{"type": "developMedication", "pathogen": "' + str(pathogenName) + '"}',
            7: '{"type": "deployMedication", "pathogen": "' + str(pathogenName) + '", "city": "' + str(infectedCity) + '"}',
            8: '{"type": "exertInfluence", "city": "' + str(city1) + '"}',
            9: '{"type": "callElections", "city": "' + str(city1) + '"}',
            10: '{"type": "applyHygienicMeasures", "city": "' + str(city1) + '"}',
            11: '{"type": "launchCampaign", "city": "' + str(city1) + '"}'
        }
        action = switcher.get(index, "Error")
        return action

    def getActionCosts(self, chosenActions):
        actionCostsArray = [
            {"first": 0, "more": 0},  # Runde beenden
            {"first": 30, "more": 10},  # Quarantäne
            {"first": 20, "more": 5},  # Flughafen schließen
            {"first": 6, "more": 3},  # Flugverbindung sperren
            {"first": 40, "more": 40},  # Impfstoff entwickeln
            {"first": 5, "more": 5},  # Impfstoff verteilen
            {"first": 20, "more": 20},  # Medikament entwickeln
            {"first": 10, "more": 10},  # Medikament verteilen
            {"first": 3, "more": 3},  # pol. Einfluss geltend machen (economy)
            {"first": 3, "more": 3},  # Neuwahlen (government)
            {"first": 3, "more": 3},  # Hygienemaßnahmen durchführen
            {"first": 3, "more": 3}  # Informationskampagne starten
        ]

        for idx, entry in enumerate(actionCostsArray):
            if idx not in chosenActions:
                actionCostsArray[idx] = entry.get("first")
            else:
                actionCostsArray[idx] = entry.get("more")
        return actionCostsArray

    def generatePossibleActionsArray(self, gameState):
        possibleActionsArray = numpy.zeros(self.ACTION_COUNT)

        possibleActionsArray[0] = 1  # Always possible
        possibleActionsArray[1] = 1  # Always possible
        possibleActionsArray[2] = 1  # Always possible
        possibleActionsArray[3] = 1  # Always possible
        possibleActionsArray[4] = self.boolToNumber(
            self.isUntreatedPathogen(gameState, "vaccineAvailable", self.EVENTCONTEXT.GLOBAL))
        possibleActionsArray[5] = self.boolToNumber(
            self.isEventPresent(gameState, "vaccineAvailable", self.EVENTCONTEXT.GLOBAL))
        possibleActionsArray[6] = self.boolToNumber(
            self.isUntreatedPathogen(gameState, "medicationAvailable", self.EVENTCONTEXT.GLOBAL))
        possibleActionsArray[7] = self.boolToNumber(
            self.isEventPresent(gameState, "medicationAvailable", self.EVENTCONTEXT.GLOBAL))
        possibleActionsArray[8] = self.boolToNumber(self.isCitiesWithAttributeNotMax(gameState, self.CITYSTATS.ECONOMY))
        possibleActionsArray[9] = self.boolToNumber(
            self.isCitiesWithAttributeNotMax(gameState, self.CITYSTATS.GOVERNMENT))
        possibleActionsArray[10] = self.boolToNumber(
            self.isCitiesWithAttributeNotMax(gameState, self.CITYSTATS.HYGIENE))
        possibleActionsArray[11] = self.boolToNumber(
            self.isCitiesWithAttributeNotMax(gameState, self.CITYSTATS.AWARENESS))

        return possibleActionsArray

    def isUntreatedPathogen(self, gameState, eventName, eventContext):
        listOfAllPathogens = self.getListOfPathogensToEvent(gameState, "outbreak", "CITY")
        listOfPathogensWithEventPresent = self.getListOfPathogensToEvent(gameState, eventName, eventContext)
        return len(listOfPathogensWithEventPresent) < len(listOfAllPathogens)

    def getListOfPathogensToEvent(self, gameState, eventName, eventContext):
        pathogenList = []


        for data in self.getEvents(gameState, eventName, eventContext):

            event = data.get("event","")

            if event != "":
                pathogen = event.get("pathogen", "")
                if pathogen != "" and pathogen.get("name") not in pathogenList:
                    pathogenList.append(pathogen.get("name"))
        return pathogenList

    def getCities(self, gameState):
        return list(gameState["cities"].values())

    def filterCitiesByEventName(self, gameState, eventName):
        cities = self.getCities(gameState)
        citiesOutput = []
        for city in cities:
            events = city.get("events", [])
            if len(events) > 0:
                for event in events:
                    if event.get("type") == eventName:
                        citiesOutput.append(city)
        return citiesOutput

    def getEventByName(self, source, eventName):
        event = list(filter(lambda event: event.get("type") == eventName, source.get("events", [])))
        if len(event) != 0:
            return event[0]
        else:
            return {}


    def getEvents(self, gameState, eventName, eventContext):
        eventArray = []

        if eventContext == self.EVENTCONTEXT.GLOBAL:
            source = gameState
            event = self.getEventByName(source, eventName)
            if event:
                eventArray.append({"source":source, "event":event})
        elif eventContext == self.EVENTCONTEXT.CITY:
            cities = self.filterCitiesByEventName(gameState, eventName)
            for city in cities:
                source = city
                event = self.getEventByName(source, eventName)
                if event:
                    eventArray.append({"source":source, "event":event})
        return eventArray

    def isEventPresent(self, gameState, eventName, eventContext):
        events = self.getEvents(gameState, eventName, eventContext)
        return len(events) > 0

    def boolToNumber(self, boolean):
        if boolean:
            return 1
        else:
            return 0

    def getCitiesWithAttributeNotMax(self, gameObject, cityAttribute):
        cities = self.getCities(gameObject)
        citiesOutput = []

        for city in cities:
            if self.attributeConverter.parseNumValue(city[cityAttribute]) < self.MAX_STATE_CITY_STATS:
                citiesOutput.append(city)
        return citiesOutput

    def isCitiesWithAttributeNotMax(self, gameObject, cityAttribute):
        return len(self.getCitiesWithAttributeNotMax(gameObject, cityAttribute)) > 0
