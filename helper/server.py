#!/usr/bin/env python3

import asyncio
from asyncio import Future
from bottle import request, BaseRequest, Bottle
from rx.subject.subject import Subject

from helper.game import Game


class Server():
    def __init__(self, gameStateSubject: Subject, actionSubject: Subject):
        BaseRequest.MEMFILE_MAX = 10 * 1024 * 1024

        self.gameStateSubject = gameStateSubject
        self.actionSubject = actionSubject
        self.currentGameObj = {}

        asyncio.set_event_loop(asyncio.new_event_loop())

        self.actionFuture = Future()
        self.actionSubject.subscribe(lambda x: self.actionFuture.set_result(x))

        self._host = "localhost"
        self._port = 50123
        self._quiet = True
        self._app = Bottle()
        self._route()
        #Start Server
        self.start()

    def _route(self):
        self._app.route('/', method="POST", callback=lambda: asyncio.run(self._index()))

    def start(self):
        #print("Server Started")
        self._app.run(host=self._host, port=self._port, quiet=self._quiet)

    async def _index(self):
        gameState = request.json
        gameObj = Game(gameState)

        self.gameStateSubject.on_next(gameObj)


        if self.isGameFinished(gameState):
            print(gameState.get("outcome"))
            if gameState.get("outcome") == "win":
                print("_________________________________________________________________________________")
            return

        #Wait for Action Object
        await self.actionFuture
        action = self.actionFuture.result()

        #Reset Action Future
        self.actionFuture = Future()
        return action

    def isGameFinished(self, gameState):
        return gameState.get("outcome") in ["win", "loss"]