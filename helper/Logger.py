class Logger():
    def __init__(self, visible:bool):
        self.visible = visible

    def print(self, *args):
        if self.visible:
            print(*args)