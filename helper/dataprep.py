#!/usr/bin/env python3
import math
import torch

from helper.ActionHandler import ActionHandler
from helper.AttributeConverter import AttributeConverter


class DataPreparer():
    def __init__(self):
        self.attributeConverter = AttributeConverter()
        self.actionHandler:ActionHandler = ActionHandler()
        self.nearCityConstant = 50


    def _sumPopulation(self, cities):
        population = 0
        for city in cities:
            population += city.get("population")
        return population

    def _isCityInfected(self, city):
        events = city.get("events", [])
        if len(events) > 0:
            for event in events:
                if event.get("type") == "outbreak":
                    return 1
        return 0


    

    def _convertCityObjArrayToValueArray(self, cityObjArray):
        cityValuesArray = []
        for cityObj in cityObjArray:
            for k, v in cityObj.items():
                cityValuesArray.append(v)
        return cityValuesArray


    def _calcDistanceBetweenCities(self, city, city2):
        lat1 = city.get("latitude")
        long1 = city.get("longitude")
        lat2 = city2.get("latitude")
        long2 = city2.get("longitude")
        return math.sqrt(math.exp(lat1 - lat2)+math.exp(long1-long2))


    def _calculateNearCities(self, city, cities):
        nearCities = 0
        for city2 in cities:
            if city != city2:
                distance = self._calcDistanceBetweenCities(city, city2)
                if distance <= self.nearCityConstant:
                    nearCities += 1
        return nearCities


    def _convertCities(self, cities):
        overallPopulation = self._sumPopulation(cities)
        #cityObjArray = []
        cityMappingArray = []
        for city in cities:
            cityObj = {
                "nearCities" : self._calculateNearCities(city, cities),
                "population" : city.get("population") / overallPopulation,
                "infected" : self._isCityInfected(city),
                "pathogeneStrength" : 0 if not self._isCityInfected(city) else self._meanPathogeneStrength(city),
                "pathogeneCount": self._countPathogenes(city),
                "economy" : self.attributeConverter.parseNumValue(city["economy"]),
                "government" : self.attributeConverter.parseNumValue(city["government"]),
                "hygiene" : self.attributeConverter.parseNumValue(city["hygiene"]),
                "awareness" : self.attributeConverter.parseNumValue(city["awareness"])
            }
            #cityObjArray.append(cityObj)
            cityMappingArray.append({"name":city.get("name"), "obj": cityObj, "connections":city.get("connections")})

        for cityEntry in cityMappingArray:
            connectedCities = cityEntry.get("connections")
            connectedCitiesObjects = list(filter(lambda cityMapEntry: cityMapEntry.get("name") in connectedCities, cityMappingArray))

            cityObj = cityEntry.get("obj")
            if 'connectedCitiesInfected' not in cityObj.values():
                cityObj['connectedCitiesInfected'] = 0

            if 'connectedCitiesNotInfected' not in cityObj.values():
                cityObj['connectedCitiesNotInfected'] = 0

            for connectedCity in connectedCitiesObjects:
                if connectedCity.get("obj").get("infected") == 1:
                    cityObj['connectedCitiesInfected'] += 1
                else:
                    cityObj['connectedCitiesNotInfected'] += 1

        cityObjArray = list(map(lambda entry: entry.get("obj"), cityMappingArray))
        return cityObjArray



    def _meanPathogeneStrength(self, city):
        offset = 3
        events = city.get("events", [])
        outbreakEvents = list(filter(lambda event: event.get("type") == "outbreak", events))
        pathogeneCount = len(outbreakEvents)
        attributeCount = 4
        overallPathogeneStrength = 0
        for event in outbreakEvents:
            pathogen = event.get("pathogen")
            overallPathogeneStrength += self.attributeConverter.parseNumValue(pathogen.get('infectivity')) + offset     #negative effect if higher
            overallPathogeneStrength += self.attributeConverter.parseNumValue(pathogen.get('mobility')) + offset        #negative effect if higher
            overallPathogeneStrength += self.attributeConverter.parseNumValue(pathogen.get('duration')) + offset        #negative effect if higher
            overallPathogeneStrength += self.attributeConverter.parseNumValue(pathogen.get('lethality')) + offset       #negative effect if higher
        meanPathogeneStrength = overallPathogeneStrength / (pathogeneCount * attributeCount)
        return meanPathogeneStrength

    def _countPathogenes(self, city):
        events = city.get("events", [])
        outbreakEvents = list(filter(lambda event: event.get("type") == "outbreak", events))
        return len(outbreakEvents)


    def convertGameDataToTensor(self, game, chosenActionsArray):
        tensorArray = []

        #INPUTS
        points = game.get("points")
        cities = game.get("cities").values()
        cityValues = self._convertCityObjArrayToValueArray(self._convertCities(cities))
        actionCosts = self.actionHandler.getActionCosts(chosenActionsArray)
        possibleActions = self.actionHandler.generatePossibleActionsArray(game)



        #TENSOR
        tensorArray.append(points)
        tensorArray.extend(cityValues)
        tensorArray.extend(actionCosts)
        tensorArray.extend(possibleActions)

        tensor = torch.Tensor(tensorArray)


        #OUTPUTS
        actionsOutput = []
        citiesOutput = []

        return tensor