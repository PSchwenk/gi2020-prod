from helper.dataprep import DataPreparer


class Game():
    def __init__(self, gameState):
        self.gameState = gameState
        self.dataPreparer = DataPreparer()

    def getGameState(self):
        return self.gameState

    def getGameTensor(self):
        return self.dataPreparer.convertGameDataToTensor(self.gameState, [])
