class ReplayMemory(object):
    def __init__(self, transition):
        self.memory = []
        self.transition = transition

    def push(self, *args):
        self.memory.append(self.transition(*args))

    def sample(self, batch_size):
        return self.memory[-1*batch_size:]

    def clear(self):
        self.memory = []

    def __len__(self):
        return len(self.memory)